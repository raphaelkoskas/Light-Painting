# Light Painting with a Robot

Dans ce projet, vous trouverez :

- Des fichiers ino pour le contrôle de moteurs à courant continu.

- Des fichiers ino pour le contrôle de moteurs pas à pas.

Pour ces différents codes, vous pourrez trouver les réalisations Light Painting associées.

Le fichier Excel nomenclature.xlsx donne des précisions sur l'ensemble des réalisations Light Painting (photos et vidéos).


## Conseils d'utilisation

1 - Télécharger le logiciel Arduino IDE

2 - Installer les deux librairies : motor et Encoder.
Pour cela il faut télécharger le répertoire Arduino et copier les deux dossiers motor et Encoder dans Documents/Arduino/librairies.

3 - Ouvrir le fichier souhaité avec Arduino IDE.
Si vous souhaitez ouvrir le code d'une forme particulière, télécharger le fichier du répertoire correspondant à la forme.




## Description du repertoire CC

Contient les différentes versions du code de contrôle des moteurs à courant continu.

Les différences entre versions sont détaillées dans le README.md du repertoire.

Le fichier Excel tuning.xlsx est un recueil de valeurs expérimentales.

À l'aide des graphs, ces valeurs ont permis de déterminer les coefficients du PID implémenté (à partir de CC_V07).

N.B. : les dossiers FORMES, LIBRARIES et SIMULATION/SPIRALE doivent être déplacé dans le dossier CC.


### /FORMES/

Contient les suites d'appel de fonctions pour effectuer différentes formes.

Ces différents code ont ainsi permis plusieurs réalisations (cf. les photos dans chaque sous-dossier).

On trouvera alors les différentes images Light Painting, résultats des figures implémentées, notamment :

- La Pokeball

- La Mandala

- Le E

- La Spirale

- La bordure

- Le Nuage

- Le triangle




## Description du repertoire PP

Ce repertoire contient les deux versions du code de contrôle des moteurs pas à pas.


### /PP/PP_SINGLE_IMAGE

Contient le code pour le contrôle des moteurs pas à pas avec une seule image.


### /PP/PP_MULTIPLE_IMAGES

Contient le code pour le contrôle des moteurs pas à pas avec plusieurs images.


### PP/Examples

Contient les différentes réalisations Light Painting implémentées en PP, notamment :

- Le Cube

- Le Papillon


### PP/Traitement_images

Contient tous les fichiers relatif au traitement d'image.
